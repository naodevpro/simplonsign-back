import { createContainer, asClass, Lifetime, asValue } from 'awilix';
import express, { Router } from 'express';
import cors from 'cors';
import path from 'path';
import cookieParser from 'cookie-parser';
import csurf from 'csurf';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import multer from 'multer';
import nodemailer from 'nodemailer';
import hbs from 'nodemailer-express-handlebars';

import Server from './config/server';
import config from './config/environment';
import db from './config/database/models';

import { ApiError, handleError } from './helpers/error';
import responseHandler from './helpers/response';
import JwtService from './libs/JwtService';
import MailService from './libs/MailService';
import LinkService from './libs/LinkService';

const container = createContainer();

const router = Router();

const jwtService = new JwtService(jwt, config.jwt_secret);
const mailing = new MailService(nodemailer, hbs);
const linkService = new LinkService(jwtService);

const csrfMiddleware = csurf({ cookie: true });
const storage = multer.diskStorage({});
const upload = multer({ storage: storage });

// db.sequelize.sync({
//   alter: true
// });

container.register({
  config: asValue(config),
  db: asValue(db),
  express: asValue(express),
  router: asValue(router),
  cors: asValue(cors),
  path: asValue(path),
  ApiError: asValue(ApiError),
  handleError: asValue(handleError),
  responseHandler: asValue(responseHandler),
  jwtService: asValue(jwtService),
  mailService: asValue(mailing),
  linkService: asValue(linkService),
  cookieParser: asValue(cookieParser),
  csrfMiddleware: asValue(csrfMiddleware),
  jwt: asValue(jwt),
  bcrypt: asValue(bcrypt),
  upload: asValue(upload)
});

container.loadModules(
  [
    'modules/**/*!(Dao$).js',
    'middlewares/*!(index).js',
    'libs/*!(index).js',
    'helpers/*!(index).js'
  ],
  {
    resolverOptions: {
      lifetime: Lifetime.SINGLETON
    },
    cwd: __dirname
  }
);

container.loadModules(['modules/**/*Dao.js'], {
  resolverOptions: {
    lifetime: Lifetime.SINGLETON,
    register: asValue
  },
  cwd: __dirname
});

const routesName = Object.keys(container.registrations).filter((item) =>
  item.match(/Router$/g)
);

const routes = routesName.map((route) => container.resolve(route));

container.register({
  routes: asValue(routes),
  server: asClass(Server).singleton()
});

export default container;
