class LinkService {
  constructor(jwtService) {
    this.jwtService = jwtService;
  }

  async generateLink(student, promo) {
    const token = await this.jwtService.generateTokenLinkPresence(student);
    const link =
      process.env.NODE_ENV === 'development'
        ? `http://localhost:8000/link/${promo}/session/${token}`
        : `https://simplonsign.herokuapp.com/link$/${promo}/session/${token}`;

    return link;
  }

  async decodeLink(token) {
    return await this.jwtService.decodeToken(token);
  }
}
export default LinkService;
