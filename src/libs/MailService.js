class MailService {
  constructor(nodemailer, hbs) {
    this.nodemailer = nodemailer;
    this.hbs = hbs;
  }

  getTransporter() {
    const transporter = this.nodemailer.createTransport({
      host: 'pro2.mail.ovh.net',
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: 'contact@alliancenouvellefrance.fr',
        pass: 'Boutiques1234@!'
      }
    });
    transporter.use(
      'compile',
      this.hbs({
        extName: '.handlebars',
        viewEngine: {
          layoutsDir: __dirname + '/../../views/layouts/',
          extname: '.handlebars'
        },
        viewPath: 'views'
      })
    );
    return transporter;
  }

  getEmailData(to, subject, template, contextVariables) {
    const emailData = {
      to: to,
      from: 'contact@alliancenouvellefrance.fr',
      subject: subject,
      template: template,
      context: {
        link: contextVariables.link,
        code: contextVariables.code,
        promotion: contextVariables.promotion,
        lastname: contextVariables.firstname,
        firstname: contextVariables.firstname
      }
    };
    return emailData;
  }

  async generateEmail(to, subject, template, contextVariables) {
    try {
      const emailData = this.getEmailData(
        to,
        subject,
        template,
        contextVariables
      );
      const transporter = this.getTransporter();
      await transporter.sendMail(emailData);
    } catch (err) {
      console.error(err);
    }
  }
}
export default MailService;
