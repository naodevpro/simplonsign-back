class AuthMiddleWare {
  constructor({ jwtService, ApiError }) {
    this.jwt = jwtService;
    this.apiError = ApiError;
  }

  isAuthentificated = async (request, response, next) => {
    try {
      const token = request.cookies['auth-cookie'];
      if (!token) {
        throw new this.apiError(
          401,
          'Votre session a expirée. Veuillez vous reconnecter 😣'
        );
      }

      const decoded = await this.jwt.decodeToken(token);

      request.currentUserId = decoded.id;
      next();
    } catch (err) {
      next(err);
    }
  };

  isDirector = async (request, response, next) => {
    try {
      const token = request.cookies['auth-cookie'];
      const decoded = await this.jwt.decodeToken(token);
      if (decoded.data.role !== 'director') {
        throw new this.apiError(
          401,
          "Vous n'avez pas les droits pour effectuer cette requête lié au directeur😣"
        );
      }
      request.currentUserId = decoded.id;
      next();
    } catch (err) {
      next(err);
    }

    isPromotionManager = async (request, response, next) => {
      try {
        const token = request.cookies['auth-cookie'];
        const decoded = await this.jwt.decodeToken(token);
        if (decoded.data.role !== 'promotionManager') {
          throw new this.apiError(
            401,
            "Vous n'avez pas les droits pour effectuer cette requête lié aux responsables de promotions"
          );
        }
        request.currentUserId = decoded.id;
        next();
      } catch (err) {
        next(err);
      }
    };

    isTeacher = async (request, response, next) => {
      try {
        const token = request.cookies['auth-cookie'];
        const decoded = await this.jwt.decodeToken(token);
        if (decoded.data.role !== 'teacher') {
          throw new this.apiError(
            401,
            "Vous n'avez pas les droits pour effectuer cette requête lié au formateur"
          );
        }
        request.currentUserId = decoded.id;
        next();
      } catch (err) {
        next(err);
      }
    };

    isStudent = async (request, response, next) => {
      try {
        const token = request.cookies['auth-cookie'];
        const decoded = await this.jwt.decodeToken(token);
        if (decoded.data.role !== 'student') {
          throw new this.apiError(
            401,
            "Vous n'avez pas les droits pour effectuer cette requête lié aux apprenants"
          );
        }
        request.currentUserId = decoded.id;
        next();
      } catch (err) {
        next(err);
      }
    };
  };
}

export default AuthMiddleWare;
