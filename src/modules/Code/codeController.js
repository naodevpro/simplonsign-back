class CodeController {
  constructor({
    codeService,
    jwtService,
    mailService,
    linkService,
    responseHandler
  }) {
    this.codeService = codeService;
    this.jwt = jwtService;
    this.mailService = mailService;
    this.linkService = linkService;
    this.responseHandler = responseHandler;
  }

  create = async (request, response, next) => {
    try {
      const usersMails = request.body;
      const sessionId = request.params.sessionId;
      usersMails.map(async (userMail) => {
        let code = Math.floor(Math.random() * 90000) + 10000;
        const informations = await this.codeService.create(
          code,
          sessionId,
          userMail
        );
        const link = await this.linkService.generateLink(
          informations.student,
          informations.student.Promotion
        );
        await this.mailService.generateEmail(
          userMail,
          'Signe freroooo',
          'donation',
          {
            link: link,
            code: code,
            promotion: informations.student.Promotion,
            lastname: informations.student.lastname,
            firstname: informations.student.firstname
          }
        );
      });
      this.responseHandler(response, 201, `Les code ont bien été envoyés ! 💥`);
    } catch (err) {
      next(err);
    }
  };

  updateCodeById = async (request, response, next) => {
    try {
      let code = await this.codeService.updateCodeById(request.params.id, {
        content: request.body.content,
        sessionId: request.body.sessionId
      });
      this.responseHandler(
        response,
        200,
        code,
        'La code a bien été mise à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  deleteCodeById = async (request, response, next) => {
    try {
      let code = this.codeService.deleteCodeById(request.params.id);
      this.responseHandler(
        response,
        200,
        code,
        'La code à bien été supprimé ✅ '
      );
    } catch (err) {
      next(err);
    }
  };

  getAllCodes = async (request, response, next) => {
    try {
      let codes = await this.codeService.getAllCodes();
      this.responseHandler(response, 200, codes, 'Toutes les codes ✅');
    } catch (err) {
      next(err);
    }
  };

  getCodeById = async (request, response, next) => {
    try {
      let code = await this.codeService.getCodeById(request.params.id);
      this.responseHandler(response, 200, code);
    } catch (err) {
      next(err);
    }
  };
  getValidityCodeLink = async (request, response, next) => {
    try {
      const { token } = request.body;
      const tokenValidity = await this.linkService.decodeLink(token);
      return this.responseHandler(response, 200, tokenValidity);
    } catch (err) {
      next(err);
    }
  };
}

export default CodeController;
