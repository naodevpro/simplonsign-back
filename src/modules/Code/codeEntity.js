class CodeEntity {
  constructor(content, sessionId) {
    this.content = content;
    this.sessionId = sessionId;
  }

  checkFieldsValues() {
    if (!this.content) {
      return false;
    } else {
      return true;
    }
  }

  checkSessionIdExisting() {
    if (!this.sessionId) {
      return false;
    } else {
      return true;
    }
  }
}

export default CodeEntity;
