class CodeRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createCode(code, sessionId, userMail) {
    const codeIsExist = await this.db.Code.findOne({
      where: { content: code }
    });
    if (codeIsExist) {
      throw new this.apiError(400, 'Un code existe déjà aves ces chiffres ❌');
    } else {
      let studentIsExist = await this.db.Student.findOne({
        attributes: ['id', 'lastname', 'firstname'],
        where: { email: userMail },
        include: [
          {
            model: this.db.Promotion,
            attributes: ['name'],
            required: true
          }
        ]
      });
      if (!studentIsExist) {
        throw new this.apiError(
          400,
          `L'utilisateur avec l'email : ${studentIsExist} n'existe pas ❌`
        );
      }

      const codeCreated = await this.db.Code.create({
        content: code,
        sessionId: sessionId
      });

      await this.db.Student.update(
        {
          currentCode: code
        },
        { where: { email: userMail } }
      );

      studentIsExist.dataValues.Promotion =
        studentIsExist.dataValues.Promotion.dataValues.name;
      return {
        student: studentIsExist.dataValues,
        code: codeCreated.dataValues
      };
    }
  }

  async updateCodeById(id, code) {
    const codeIsExist = await this.db.Code.findOne({
      where: { id: id }
    });
    if (!codeIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le code que vous souhaitez modifier n'existe pas/plus ❌ "
      );
    }
    return await this.db.Code.update(
      {
        content: code.content,
        sessionId: code.sessionId
      },
      { where: { id: id } }
    );
  }

  async deleteCodeById(id) {
    const codeIsExist = await this.db.Code.findOne({
      where: { id: id }
    });
    if (!codeIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le code que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Code.destroy({ where: { id: id } });
  }

  async getCodes() {
    const codes = await this.db.Code.findAll({
      attributes: ['content', 'sessionId']
    });
    if (!codes.length) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucun codes ❌");
    }
    return codes;
  }

  async getCodeById(id) {
    return await this.db.Code.findOne({
      attributes: ['id', 'content', 'sessionId'],
      where: { id: id }
    });
  }
}
export default CodeRepository;
