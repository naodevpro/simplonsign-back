class CodeRouter {
  constructor({ router, codeController }) {
    this.router = router;
    this.initializeRoutes({ codeController });
    return this.router;
  }

  initializeRoutes({ codeController }) {
    this.router.route('/codes/:sessionId/create/').post(codeController.create);
    this.router.route('/codes/:id').patch(codeController.updateCodeById);
    this.router.route('/codes/:id').delete(codeController.deleteCodeById);
    this.router.route('/codes').get(codeController.getAllCodes);
    this.router.route('/codes/:id').get(codeController.getCodeById);
    this.router.route('/codes/me').post(codeController.getValidityCodeLink);
  }
}
export default CodeRouter;
