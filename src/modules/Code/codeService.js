import CodeEntity from './codeEntity';

class CodeService {
  constructor({ codeRepository, ApiError }) {
    this.codeRepository = codeRepository;
    this.apiError = ApiError;
  }

  async create(code, sessionId, userMail) {
    const codeEntity = new CodeEntity(code, sessionId);
    if (!codeEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!codeEntity.checkSessionIdExisting()) {
      throw new this.apiError(
        400,
        'Veuillez ajouter une session lié au code! ❌'
      );
    }
    return await this.codeRepository.createCode(code, sessionId, userMail);
  }

  async updateCodeById(id, code) {
    return await this.codeRepository.updateCodeById(id, code);
  }

  async deleteCodeById(id) {
    return await this.codeRepository.deleteCodeById(id);
  }

  async getAllCodes() {
    return await this.codeRepository.getCodes();
  }

  async getCodeById(id) {
    return await this.codeRepository.getCodeById(id);
  }
}

export default CodeService;
