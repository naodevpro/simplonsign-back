class DirectorController {
  constructor({ directorService, jwtService, responseHandler }) {
    this.directorService = directorService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  register = async (request, response, next) => {
    try {
      let director = await this.directorService.register({ ...request.body });
      this.responseHandler(
        response,
        201,
        director,
        `Bienvenue ${director.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  login = async (request, response, next) => {
    try {
      let director = await this.directorService.login({ ...request.body });
      let token = await this.jwt.generateToken({
        id: director.dataValues.id,
        firstname: director.firstname,
        lastname: director.lastname,
        email: director.dataValues.email,
        role: director.dataValues.role
      });
      response.cookie('auth-cookie', token, {
        secure: true,
        maxAge: 3600000,
        sameSite: 'None'
      });
      this.responseHandler(
        response,
        200,
        {
          id: director.dataValues.id,
          lastname: director.dataValues.lastname,
          firstname: director.dataValues.firstname,
          email: director.dataValues.email,
          role: director.dataValues.role,
          token
        },
        `Bonjour ${director.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  updateDirectorById = async (request, response, next) => {
    try {
      let director = await this.directorService.updateDirectorById(
        request.params.id,
        {
          role: request.body.role,
          lastname: request.body.lastname,
          firstname: request.body.firstname,
          email: request.body.email,
          gender: request.body.gender
        }
      );
      this.responseHandler(
        response,
        200,
        director,
        'Le profil a bien été mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  deleteDirectorById = async (request, response, next) => {
    try {
      let director = await this.directorService.deleteDirectorById(
        request.params.id
      );
      this.responseHandler(
        response,
        200,
        director,
        'Le profil à bien été supprimé ✅ '
      );
    } catch (err) {
      next(err);
    }
  };

  getAllDirectors = async (request, response, next) => {
    try {
      let directors = await this.directorService.getAllDirectors();
      this.responseHandler(response, 200, directors, 'Tous les directeurs ✅');
    } catch (err) {
      next(err);
    }
  };

  getDirectorById = async (request, response, next) => {
    try {
      let director = await this.directorService.getDirectorById(
        request.params.id
      );
      this.responseHandler(response, 200, director);
    } catch (err) {
      next(err);
    }
  };
}

export default DirectorController;
