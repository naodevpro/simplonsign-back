class DirectorRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createDirector(director) {
    const salt = this.bcrypt.genSaltSync(10);
    director.password = this.bcrypt.hashSync(director.password, salt);
    const directorIsExist = await this.db.Director.findOne({
      where: { email: director.email }
    });
    if (directorIsExist) {
      throw new this.apiError(
        400,
        'Un directeur existe déjà sous cet adresse e-mail ❌'
      );
    } else {
      return await this.db.Director.create(director);
    }
  }

  async checkCredentials(credentials) {
    const directorIsExist = await this.db.Director.findOne({
      where: { email: credentials.email }
    });
    if (!directorIsExist) {
      throw new this.apiError(
        400,
        'Il ne semble pas y avoir de compte sous cet adresse-email ❌'
      );
    } else {
      let checkPassword = await this.bcrypt.compareSync(
        credentials.password,
        directorIsExist.password
      );
      if (!checkPassword) {
        throw new this.apiError(400, 'Mot de passe incorrect ❌');
      }
      return directorIsExist;
    }
  }

  async updateDirectorById(id, director) {
    const directorIsExist = await this.db.Director.findOne({
      where: { id: id }
    });
    if (!directorIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le profil du directeur que vous voulez modifier n'existe pas ❌ "
      );
    }
    return await this.db.Director.update(
      {
        role: director.role,
        lastname: director.lastname,
        firstname: director.firstname,
        email: director.email,
        gender: director.gender
      },
      { where: { id: id } }
    );
  }

  async deleteDirectorById(id) {
    const directorIsExist = await this.db.Director.findOne({
      where: { id: id }
    });
    if (!directorIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le profil du directeur que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Director.destroy({ where: { id: id } });
  }

  async getDirectors() {
    const directors = await this.db.Director.findAll({
      attributes: ['id', 'role', 'firstname', 'lastname', 'email', 'gender']
    });
    if (!directors.length) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucun directeurs ❌");
    }
    return directors;
  }

  async getDirectorById(id) {
    return await this.db.Director.findOne({
      attributes: ['id', 'role', 'firstname', 'lastname', 'email', 'gender'],
      where: { id: id }
    });
  }
}
export default DirectorRepository;
