class DirectorRouter {
  constructor({ router, directorController }) {
    this.router = router;
    this.initializeRoutes({ directorController });
    return this.router;
  }

  initializeRoutes({ directorController }) {
    this.router.route('/directors/register').post(directorController.register);
    this.router.route('/directors/login').post(directorController.login);
    this.router
      .route('/directors/:id')
      .patch(directorController.updateDirectorById);
    this.router
      .route('/directors/:id')
      .delete(directorController.deleteDirectorById);
    this.router.route('/directors').get(directorController.getAllDirectors);
    this.router.route('/directors/:id').get(directorController.getDirectorById);
  }
}
export default DirectorRouter;
