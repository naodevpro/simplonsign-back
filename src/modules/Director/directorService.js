import DirectorEntity from './directorEntity';

class DirectorService {
  constructor({ directorRepository, ApiError }) {
    this.directorRepository = directorRepository;
    this.apiError = ApiError;
  }

  async register(director) {
    const directorEntity = new DirectorEntity(director);
    if (!directorEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!directorEntity.checkRole()) {
      throw new this.apiError(400, 'le role est incorrect ! ❌');
    }
    if (!directorEntity.checkEmail()) {
      throw new this.apiError(400, "l'email n'est pas dans le bon format ! ❌");
    }
    if (!directorEntity.checkPassword()) {
      throw new this.apiError(
        400,
        'le mot de passe doit contenir entre 8 et 15 caractères, une majuscule, un chiffre et un caractère spécial ! ❌'
      );
    }
    if (!directorEntity.checkGender()) {
      throw new this.apiError(400, 'le genre est incorrect ! ❌');
    }
    return await this.directorRepository.createDirector(director);
  }

  async login(credentials) {
    return await this.directorRepository.checkCredentials(credentials);
  }

  async updateDirectorById(id, director) {
    return await this.directorRepository.updateDirectorById(id, director);
  }

  async deleteDirectorById(id) {
    return await this.directorRepository.deleteDirectorById(id);
  }

  async getAllDirectors() {
    return await this.directorRepository.getDirectors();
  }

  async getDirectorById(id) {
    return await this.directorRepository.getDirectorById(id);
  }
}

export default DirectorService;
