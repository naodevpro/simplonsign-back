class JustifiedAbsenceController {
  constructor({ justifiedAbsenceService, jwtService, responseHandler }) {
    this.justifiedAbsenceService = justifiedAbsenceService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  create = async (request, response, next) => {
    try {
      let justifiedAbsence = await this.justifiedAbsenceService.create({
        ...request.body
      });
      this.responseHandler(
        response,
        201,
        justifiedAbsence,
        `Nouvelle absence justifiée ajoutée 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  updateJustifiedAbsenceById = async (request, response, next) => {
    try {
      let justifiedAbsence =
        await this.justifiedAbsenceService.updateJustifiedAbsenceById(
          request.params.id,
          {
            date: request.body.date,
            attachment: request.body.attachment,
            studentId: request.body.studentId,
            promotionManagerId: request.body.promotionManagerId,
            sessionId: request.body.sessionId
          }
        );
      this.responseHandler(
        response,
        200,
        justifiedAbsence,
        "L'absence justifié a bien été mise à jour ✅"
      );
    } catch (err) {
      next(err);
    }
  };

  deleteJustifiedAbsenceById = async (request, response, next) => {
    try {
      let justifiedAbsence =
        this.justifiedAbsenceService.deleteJustifiedAbsenceById(
          request.params.id
        );
      this.responseHandler(
        response,
        200,
        justifiedAbsence,
        "L'absence justifiée à bien été supprimée ✅"
      );
    } catch (err) {
      next(err);
    }
  };

  getAllJustifiedAbsences = async (request, response, next) => {
    try {
      let justifiedAbsences =
        await this.justifiedAbsenceService.getAllJustifiedAbsences();
      this.responseHandler(
        response,
        200,
        justifiedAbsences,
        'Toutes les absence justifiées ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  getJustifiedAbsenceById = async (request, response, next) => {
    try {
      let justifiedAbsence =
        await this.justifiedAbsenceService.getJustifiedAbsenceById(
          request.params.id
        );
      this.responseHandler(response, 200, justifiedAbsence);
    } catch (err) {
      next(err);
    }
  };
}

export default JustifiedAbsenceController;
