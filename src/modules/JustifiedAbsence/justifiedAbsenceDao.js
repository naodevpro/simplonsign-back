'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class JustifiedAbsence extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Student, {
        foreignKey: 'studentId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.PromotionManager, {
        foreignKey: 'promotionManagerId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Session, {
        foreignKey: 'sessionId',
        onDelete: 'CASCADE'
      });
    }
  }
  JustifiedAbsence.init(
    {
      date: DataTypes.STRING,
      attachment: DataTypes.STRING,
      studentId: DataTypes.UUID,
      promotionManagerId: DataTypes.UUID,
      sessionId: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'JustifiedAbsence'
    }
  );
  return JustifiedAbsence;
};
