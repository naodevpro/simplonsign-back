class JustifiedAbsenceEntity {
  constructor({ date, attachment, studentId, promotionManagerId, sessionId }) {
    this.date = date;
    this.attachment = attachment;
    this.studentId = studentId;
    this.promotionManagerId = promotionManagerId;
    this.sessionId = promotiosessionIdnManagerId;
  }

  checkFieldsValues() {
    if (
      !this.date ||
      !this.attachment ||
      !this.studentId ||
      !this.promotionManagerId
    ) {
      return false;
    } else {
      return true;
    }
  }

  checkStudentIdExisting() {
    if (!this.studentId) {
      return false;
    } else {
      return true;
    }
  }

  checkPromotionManagerIdExisting() {
    if (!this.promotionManagerId) {
      return false;
    } else {
      return true;
    }
  }

  checkSessionIdExisting() {
    if (!this.sessionId) {
      return false;
    } else {
      return true;
    }
  }
}

export default JustifiedAbsenceEntity;
