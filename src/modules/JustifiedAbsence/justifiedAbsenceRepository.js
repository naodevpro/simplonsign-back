class JustifiedAbsenceRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createJustifiedAbsence(justifiedAbsence) {
    const justifiedAbsenceIsExist = await this.db.JustifiedAbsence.findOne({
      where: {
        date: justifiedAbsence.date,
        sessionId: justifiedAbsence.sessionId,
        studentId: justifiedAbsence.studentId
      }
    });
    if (justifiedAbsenceIsExist) {
      throw new this.apiError(
        400,
        'Une absence justifiée existe déjà à la même date'
      );
    } else {
      return await this.db.JustifiedAbsence.create(justifiedAbsence);
    }
  }

  async updateJustifiedAbsenceById(id, justifiedAbsence) {
    const justifiedAbsenceIsExist = await this.db.JustifiedAbsence.findOne({
      where: {
        date: justifiedAbsence.date,
        sessionId: justifiedAbsence.sessionId,
        studentId: justifiedAbsence.studentId
      }
    });
    if (!justifiedAbsenceIsExist) {
      throw new this.apiError(
        400,
        "Il semble que l'absence justifiée que vous souhaitez modifier n'existe pas/plus ❌ "
      );
    }
    return await this.db.JustifiedAbsence.update(
      {
        date: justifiedAbsence.date,
        attachment: justifiedAbsence.attachment,
        studentId: justifiedAbsence.studentId,
        promotionManagerId: justifiedAbsence.promotionManagerId
      },
      { where: { id: id } }
    );
  }

  async deleteJustifiedAbsenceById(id) {
    const justifiedAbsenceIsExist = await this.db.JustifiedAbsence.findOne({
      where: { id: id }
    });
    if (!justifiedAbsenceIsExist) {
      throw new this.apiError(
        400,
        "Il semble que l'absence justifiée que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.JustifiedAbsence.destroy({ where: { id: id } });
  }

  async getJustifiedAbsences() {
    const justifiedAbsences = await this.db.JustifiedAbsence.findAll({
      attributes: ['date', 'attachment', 'studentId', 'promotionManagerId']
    });
    if (!justifiedAbsences.length) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y a aucune absences justifiées ❌"
      );
    }
    return justifiedAbsences;
  }

  async getJustifiedAbsenceById(id) {
    return await this.db.JustifiedAbsence.findOne({
      attributes: ['date', 'attachment', 'studentId', 'promotionManagerId'],
      where: { id: id }
    });
  }
}
export default JustifiedAbsenceRepository;
