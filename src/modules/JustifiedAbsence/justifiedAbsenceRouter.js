class JustifiedAbsenceRouter {
  constructor({ router, justifiedAbsenceController }) {
    this.router = router;
    this.initializeRoutes({ justifiedAbsenceController });
    return this.router;
  }

  initializeRoutes({ justifiedAbsenceController }) {
    this.router
      .route('/justifiedAbsences/create')
      .post(justifiedAbsenceController.create);
    this.router
      .route('/justifiedAbsences/:id')
      .patch(justifiedAbsenceController.updateJustifiedAbsenceById);
    this.router
      .route('/justifiedAbsences/:id')
      .delete(justifiedAbsenceController.deleteJustifiedAbsenceById);
    this.router
      .route('/justifiedAbsences')
      .get(justifiedAbsenceController.getAllJustifiedAbsences);
    this.router
      .route('/justifiedAbsences/:id')
      .get(justifiedAbsenceController.getJustifiedAbsenceById);
  }
}
export default JustifiedAbsenceRouter;
