import JustifiedAbsenceEntity from './justifiedAbsenceEntity';

class JustifiedAbsenceService {
  constructor({ justifiedAbsenceRepository, ApiError }) {
    this.justifiedAbsenceRepository = justifiedAbsenceRepository;
    this.apiError = ApiError;
  }

  async create(justifiedAbsence) {
    const justifiedAbsenceEntity = new JustifiedAbsenceEntity(justifiedAbsence);
    if (!justifiedAbsenceEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!justifiedAbsenceEntity.checkStudentIdExisting()) {
      throw new this.apiError(
        400,
        "Veuillez ajouter un/e apprenant.e lié/e à l'absence justifiée ! ❌"
      );
    }
    if (!justifiedAbsenceEntity.checkPromotionManagerIdExisting()) {
      throw new this.apiError(
        400,
        "Veuillez ajouter un/e responsable de promotion lié/e à l'absence justifiée ! ❌"
      );
    }
    if (!justifiedAbsenceEntity.checkSessionIdExisting()) {
      throw new this.apiError(
        400,
        "Veuillez ajouter une session liée à l'absence justifiée ! ❌"
      );
    }
    return await this.justifiedAbsenceRepository.createJustifiedAbsence(
      justifiedAbsence
    );
  }

  async updateJustifiedAbsenceById(id, justifiedAbsence) {
    return await this.justifiedAbsenceRepository.updateJustifiedAbsenceById(
      id,
      justifiedAbsence
    );
  }

  async deleteJustifiedAbsenceById(id) {
    return await this.justifiedAbsenceRepository.deleteJustifiedAbsenceById(id);
  }

  async getAllJustifiedAbsences() {
    return await this.justifiedAbsenceRepository.getJustifiedAbsences();
  }

  async getJustifiedAbsenceById(id) {
    return await this.justifiedAbsenceRepository.getJustifiedAbsenceById(id);
  }
}

export default JustifiedAbsenceService;
