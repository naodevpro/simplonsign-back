class PresenceController {
  constructor({ presenceService, jwtService, responseHandler }) {
    this.presenceService = presenceService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  create = async (request, response, next) => {
    try {
      let presence = await this.presenceService.create({ ...request.body });
      if (!presence) {
        return this.responseHandler(
          response,
          400,
          `La session est terminée, vous serez compté absent ! ❌`
        );
      }
      this.responseHandler(
        response,
        201,
        presence,
        `Nouvelle presence créée 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  updatePresenceById = async (request, response, next) => {
    try {
      let presence = await this.presenceService.updatePresenceById(
        request.params.id,
        {
          signedAt: request.body.signedAt,
          studentId: request.body.studentId,
          sessionId: request.body.sessionId
        }
      );
      this.responseHandler(
        response,
        200,
        presence,
        'La presence a bien été mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  deletePresenceById = async (request, response, next) => {
    try {
      let presence = this.presenceService.deletePresenceById(request.params.id);
      this.responseHandler(
        response,
        200,
        presence,
        'La presence à bien été supprimée ✅ '
      );
    } catch (err) {
      next(err);
    }
  };

  getAllPresences = async (request, response, next) => {
    try {
      let presences = await this.presenceService.getAllPresences();
      this.responseHandler(response, 200, presences, 'Toutes les présences ✅');
    } catch (err) {
      next(err);
    }
  };

  getPresenceById = async (request, response, next) => {
    try {
      let presence = await this.presenceService.getPresenceById(
        request.params.id
      );
      this.responseHandler(response, 200, presence);
    } catch (err) {
      next(err);
    }
  };
}

export default PresenceController;
