'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Presence extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Session, {
        foreignKey: 'sessionId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Student, {
        foreignKey: 'studentId',
        onDelete: 'CASCADE'
      });
    }
  }
  Presence.init(
    {
      signedAt: DataTypes.DATE,
      status: DataTypes.STRING,
      studentId: DataTypes.UUID,
      sessionId: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'Presence'
    }
  );
  return Presence;
};
