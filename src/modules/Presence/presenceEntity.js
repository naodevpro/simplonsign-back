class PresenceEntity {
  constructor({ studentId, sessionId }) {
    this.studentId = studentId;
    this.sessionId = sessionId;
  }

  checkFieldsValues() {
    if (!this.studentId || !this.sessionId) {
      return false;
    } else {
      return true;
    }
  }

  checkStudentIdExisting() {
    if (!this.studentId) {
      return false;
    } else {
      return true;
    }
  }

  checkSessionIdExisting() {
    if (!this.sessionId) {
      return false;
    } else {
      return true;
    }
  }
}

export default PresenceEntity;
