class PresenceRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createPresence(presence) {
    const session = await this.db.Student.findOne({
      where: { id: presence.sessionId }
    });

    let limitHalfday = [];
    let endSession = [];
    if (session.dataValues.halfday === 'morning') {
      limitHalfday = [10, 0];
      endSession = [12, 30];
    } else {
      limitHalfday = [14, 30];
      endSession = [18, 50];
    }

    var signedAtDate = new Date();

    var signedHour = signedAtDate.getHours();
    var signedMinute = signedAtDate.getMinutes();

    if (signedHour > limitHalfday[0] && signedHour <= endSession[0]) {
      if (signedMinute > limitHalfday[1] && signedMinute < endSession[1]) {
        return await this.db.Presence.create({
          signedAt: signedAtDate,
          status: 'retard',
          studentId: presence.studentId,
          sessionId: presence.sessionId
        });
      }
    } else if (signedHour > endSession[0]) {
      if (signedMinute > endSession[1]) {
        return null;
      }
    } else {
      return await this.db.Presence.create({
        signedAt: signedAtDate,
        status: "à l'heure",
        studentId: presence.studentId,
        sessionId: presence.sessionId
      });
    }
  }

  async updatePresenceById(id, presence) {
    const presenceIsExist = await this.db.Presence.findOne({
      where: { signedAt: presence.signedAt, studentId: presence.studentId }
    });
    if (!presenceIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la presence que vous souhaitez modifier n'existe pas/plus ❌ "
      );
    }
    return await this.db.Presence.update(
      {
        signedAt: presence.signedAt,
        status: presence.status,
        studentId: presence.studentId,
        sessionId: presence.sessionId
      },
      { where: { id: id } }
    );
  }

  async deletePresenceById(id) {
    const presenceIsExist = await this.db.Presence.findOne({
      where: { id: id }
    });
    if (!presenceIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la presence que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Presence.destroy({ where: { id: id } });
  }

  async getPresences() {
    const presences = await this.db.Presence.findAll({
      attributes: ['signedAt', 'status', 'studentId', 'sessionId']
    });
    if (!presences.length) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucune presences ❌");
    }
    return presences;
  }

  async getPresenceById(id) {
    return await this.db.Presence.findOne({
      attributes: ['signedAt', 'status', 'studentId', 'sessionId'],
      where: { id: id }
    });
  }
}
export default PresenceRepository;
