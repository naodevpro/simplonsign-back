class PresenceRouter {
  constructor({ router, presenceController }) {
    this.router = router;
    this.initializeRoutes({ presenceController });
    return this.router;
  }

  initializeRoutes({ presenceController }) {
    this.router.route('/presences/create').post(presenceController.create);
    this.router
      .route('/presences/:id')
      .patch(presenceController.updatePresenceById);
    this.router
      .route('/presences/:id')
      .delete(presenceController.deletePresenceById);
    this.router.route('/presences').get(presenceController.getAllPresences);
    this.router.route('/presences/:id').get(presenceController.getPresenceById);
  }
}
export default PresenceRouter;
