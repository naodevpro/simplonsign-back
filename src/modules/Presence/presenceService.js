import PresenceEntity from './presenceEntity';

class PresenceService {
  constructor({ presenceRepository, ApiError }) {
    this.presenceRepository = presenceRepository;
    this.apiError = ApiError;
  }

  async create(presence) {
    const presenceEntity = new PresenceEntity(presence);
    if (!presenceEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!presenceEntity.checkSessionIdExisting()) {
      throw new this.apiError(
        400,
        'Veuillez ajouter une session lié au presence! ❌'
      );
    }
    if (!presenceEntity.checkStudentIdExisting()) {
      throw new this.apiError(
        400,
        'Veuillez ajouter un/e apprenant.e lié à la présence ! ❌'
      );
    }
    return await this.presenceRepository.createPresence(presence);
  }

  async updatePresenceById(id, presence) {
    return await this.presenceRepository.updatePresenceById(id, presence);
  }

  async deletePresenceById(id) {
    return await this.presenceRepository.deletePresenceById(id);
  }

  async getAllPresences() {
    return await this.presenceRepository.getPresences();
  }

  async getPresenceById(id) {
    return await this.presenceRepository.getPresenceById(id);
  }
}

export default PresenceService;
