class PromotionController {
  constructor({ promotionService, jwtService, responseHandler }) {
    this.promotionService = promotionService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  create = async (request, response, next) => {
    try {
      let promotion = await this.promotionService.create({ ...request.body });
      this.responseHandler(
        response,
        201,
        promotion,
        `Nouvelle promotion ${promotion.dataValues.firstname} créée 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  updatePromotionById = async (request, response, next) => {
    try {
      let promotion = await this.promotionService.updatePromotionById(
        request.params.id,
        {
          avatar: request.body.avatar,
          name: request.body.name,
          promotionManagerId: request.body.promotionManagerId,
          teacherId: request.body.teacherId
        }
      );
      this.responseHandler(
        response,
        200,
        promotion,
        'La promotion a bien été mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  deletePromotionById = async (request, response, next) => {
    try {
      let promotion = this.promotionService.deletePromotionById(
        request.params.id
      );
      this.responseHandler(
        response,
        200,
        promotion,
        'La promotion à bien été supprimé ✅ '
      );
    } catch (err) {
      next(err);
    }
  };

  getAllPromotions = async (request, response, next) => {
    try {
      let promotions = await this.promotionService.getAllPromotions();
      this.responseHandler(
        response,
        200,
        promotions,
        'Toutes les promotions ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  getPromotionById = async (request, response, next) => {
    try {
      let promotion = await this.promotionService.getPromotionById(
        request.params.id
      );
      this.responseHandler(response, 200, promotion);
    } catch (err) {
      next(err);
    }
  };
}

export default PromotionController;
