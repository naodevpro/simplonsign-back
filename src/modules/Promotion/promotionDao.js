'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Promotion extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Session, {
        foreignKey: 'promotionId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Student, {
        foreignKey: 'promotionId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.PromotionManager, {
        foreignKey: 'promotionManagerId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Teacher, {
        foreignKey: 'teacherId',
        onDelete: 'CASCADE'
      });
    }
  }
  Promotion.init(
    {
      avatar: DataTypes.STRING,
      name: DataTypes.STRING,
      promotionManagerId: DataTypes.UUID,
      teacherId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Promotion'
    }
  );
  return Promotion;
};
