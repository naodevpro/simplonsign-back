class PromotionEntity {
  constructor({ avatar, name, promotionManagerId, teacherId }) {
    this.avatar = avatar;
    this.name = name;
    this.promotionManagerId = promotionManagerId;
    this.teacherId = teacherId;
  }

  checkFieldsValues() {
    if (
      !this.avatar ||
      !this.name ||
      !this.promotionManagerId ||
      !this.teacherId
    ) {
      return false;
    } else {
      return true;
    }
  }

  checkPromotionManagerIdExisting() {
    if (!this.promotionManagerId) {
      return false;
    } else {
      return true;
    }
  }

  checkTeacherIdExisting() {
    if (!this.teacherId) {
      return false;
    } else {
      return true;
    }
  }
}

export default PromotionEntity;
