class PromotionRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createPromotion(promotion) {
    const promotionIsExist = await this.db.Promotion.findOne({
      where: { name: promotion.name }
    });
    if (promotionIsExist) {
      throw new this.apiError(
        400,
        'Une promotion existe déjà sous le même nom ❌'
      );
    } else {
      return await this.db.Promotion.create(promotion);
    }
  }

  async updatePromotionById(id, promotion) {
    const promotionIsExist = await this.db.Promotion.findOne({
      where: { id: id }
    });
    const promotionNameIsExist = await this.db.Promotion.findOne({
      where: { name: promotion.name }
    });
    if (!promotionIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la promotion que vous souhaitez modifier n'existe pas/plus ❌ "
      );
    } else if (
      promotionNameIsExist &&
      promotionNameIsExist.dataValues.id !== promotionIsExist.dataValues.id
    ) {
      throw new this.apiError(
        400,
        "Il semble qu'une promotion existe déjà sous un même nom ! ❌ "
      );
    }
    return await this.db.Promotion.update(
      {
        avatar: promotion.avatar,
        name: promotion.name,
        promotionManagerId: promotion.promotionManagerId,
        teacherId: promotion.teacherId
      },
      { where: { id: id } }
    );
  }

  async deletePromotionById(id) {
    const promotionIsExist = await this.db.Promotion.findOne({
      where: { id: id }
    });
    if (!promotionIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la promotion que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Promotion.destroy({ where: { id: id } });
  }

  async getPromotions() {
    const promotions = await this.db.Promotion.findAll({
      attributes: ['id', 'avatar', 'name']
    });
    if (!promotions.length) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y a aucune promotions ❌"
      );
    }
    return promotions;
  }

  async getPromotionById(id) {
    return await this.db.Promotion.findOne({
      attributes: ['id', 'avatar', 'name', 'promotionManagerId', 'teacherId'],
      where: { id: id }
    });
  }
}
export default PromotionRepository;
