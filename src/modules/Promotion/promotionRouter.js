class PromotionRouter {
  constructor({ router, promotionController }) {
    this.router = router;
    this.initializeRoutes({ promotionController });
    return this.router;
  }

  initializeRoutes({ promotionController }) {
    this.router.route('/promotions/create').post(promotionController.create);
    this.router
      .route('/promotions/:id')
      .patch(promotionController.updatePromotionById);
    this.router
      .route('/promotions/:id')
      .delete(promotionController.deletePromotionById);
    this.router.route('/promotions').get(promotionController.getAllPromotions);
    this.router
      .route('/promotions/:id')
      .get(promotionController.getPromotionById);
  }
}
export default PromotionRouter;
