import PromotionEntity from './promotionEntity';

class PromotionService {
  constructor({ promotionRepository, ApiError }) {
    this.promotionRepository = promotionRepository;
    this.apiError = ApiError;
  }

  async create(promotion) {
    const promotionEntity = new PromotionEntity(promotion);
    if (!promotionEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!promotionEntity.checkPromotionManagerIdExisting()) {
      throw new this.apiError(
        400,
        'Veuillez ajouter un/e reponsable de promotion lié à la promotion! ❌'
      );
    }
    if (!promotionEntity.checkTeacherIdExisting()) {
      throw new this.apiError(
        400,
        'Veuillez ajouter un/e formateur.ice lié à la promotion! ❌'
      );
    }
    return await this.promotionRepository.createPromotion(promotion);
  }

  async updatePromotionById(id, promotion) {
    return await this.promotionRepository.updatePromotionById(id, promotion);
  }

  async deletePromotionById(id) {
    return await this.promotionRepository.deletePromotionById(id);
  }

  async getAllPromotions() {
    return await this.promotionRepository.getPromotions();
  }

  async getPromotionById(id) {
    return await this.promotionRepository.getPromotionById(id);
  }
}

export default PromotionService;
