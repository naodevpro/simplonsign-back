class PromotionManagerController {
  constructor({ promotionManagerService, jwtService, responseHandler }) {
    this.promotionManagerService = promotionManagerService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  register = async (request, response, next) => {
    try {
      let promotionManager = await this.promotionManagerService.register({
        ...request.body
      });
      this.responseHandler(
        response,
        201,
        promotionManager,
        `Bienvenue ${promotionManager.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  login = async (request, response, next) => {
    try {
      let promotionManager = await this.promotionManagerService.login({
        ...request.body
      });
      let token = await this.jwt.generateToken({
        id: promotionManager.dataValues.id,
        firstname: promotionManager.firstname,
        lastname: promotionManager.lastname,
        email: promotionManager.dataValues.email,
        role: promotionManager.dataValues.role
      });
      response.cookie('auth-cookie', token, {
        secure: true,
        maxAge: 3600000,
        sameSite: 'None'
      });
      this.responseHandler(
        response,
        200,
        {
          id: promotionManager.dataValues.id,
          lastname: promotionManager.dataValues.lastname,
          firstname: promotionManager.dataValues.firstname,
          email: promotionManager.dataValues.email,
          role: promotionManager.dataValues.role,
          token
        },
        `Bonjour ${promotionManager.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  updatePromotionManagerById = async (request, response, next) => {
    try {
      let promotionManager =
        await this.promotionManagerService.updatePromotionManagerById(
          request.params.id,
          {
            role: request.body.role,
            lastname: request.body.lastname,
            firstname: request.body.firstname,
            email: request.body.email,
            gender: request.body.gender
          }
        );
      this.responseHandler(
        response,
        200,
        promotionManager,
        'Le profil a bien été mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  deletePromotionManagerById = async (request, response, next) => {
    try {
      let promotionManager =
        await this.promotionManagerService.deletePromotionManagerById(
          request.params.id
        );
      this.responseHandler(
        response,
        200,
        promotionManager,
        'Le profil à bien été supprimé ✅ '
      );
    } catch (err) {
      next(err);
    }
  };

  getAllPromotionManagers = async (request, response, next) => {
    try {
      let promotionManager =
        await this.promotionManagerService.getAllPromotionManagers();
      this.responseHandler(
        response,
        200,
        promotionManager,
        'Tous les Responsable de formations ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  getPromotionManagerById = async (request, response, next) => {
    try {
      let promotionManager =
        await this.promotionManagerService.getPromotionManagerById(
          request.params.id
        );
      this.responseHandler(response, 200, promotionManager);
    } catch (err) {
      next(err);
    }
  };
}

export default PromotionManagerController;
