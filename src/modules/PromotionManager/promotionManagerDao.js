'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PromotionManager extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Promotion, {
        foreignKey: 'promotionManagerId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Teacher, {
        foreignKey: 'promotionManagerId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.JustifiedAbsence, {
        foreignKey: 'promotionManagerId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Director, {
        foreignKey: 'directorId',
        onDelete: 'CASCADE'
      });
    }
  }
  PromotionManager.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      role: DataTypes.STRING,
      avatar: DataTypes.STRING,
      firstname: DataTypes.STRING,
      lastname: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      gender: DataTypes.STRING,
      directorId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'PromotionManager'
    }
  );
  return PromotionManager;
};
