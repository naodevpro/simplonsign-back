class PromotionManagerEntity {
  constructor({
    role,
    avatar,
    firstname,
    lastname,
    email,
    password,
    gender,
    directorId
  }) {
    this.role = role;
    this.avatar = avatar;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.password = password;
    this.gender = gender;
    this.directorId = directorId;
  }

  checkFieldsValues() {
    if (
      !this.role ||
      !this.avatar ||
      !this.firstname ||
      !this.lastname ||
      !this.email ||
      !this.password ||
      !this.gender
    ) {
      return false;
    } else {
      return true;
    }
  }

  checkRole() {
    if (this.role !== 'promotionManager') {
      return false;
    } else {
      return true;
    }
  }

  checkEmail() {
    const emailRegexp =
      /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!emailRegexp.test(this.email)) {
      return false;
    } else {
      return true;
    }
  }

  checkPassword() {
    const passwordRegexp =
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (!passwordRegexp.test(this.password)) {
      return false;
    } else {
      return true;
    }
  }

  checkGender() {
    switch (this.gender) {
      case 'masculin':
        return true;
      case 'female':
        return true;
      default:
        return false;
    }
  }

  checkDirectorIdExisting() {
    if (!this.directorId) {
      return false;
    } else {
      return true;
    }
  }
}

export default PromotionManagerEntity;
