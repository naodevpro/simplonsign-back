class PromotionManagerRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createPromotionManager(promotionManager) {
    const salt = this.bcrypt.genSaltSync(10);
    promotionManager.password = this.bcrypt.hashSync(
      promotionManager.password,
      salt
    );
    const promotionManagerIsExist = await this.db.PromotionManager.findOne({
      where: { email: promotionManager.email }
    });
    if (promotionManagerIsExist) {
      throw new this.apiError(
        400,
        'Un responsable de formation existe déjà sous cet adresse e-mail ❌'
      );
    } else {
      return await this.db.PromotionManager.create(promotionManager);
    }
  }

  async checkCredentials(credentials) {
    const promotionManagerIsExist = await this.db.PromotionManager.findOne({
      where: { email: credentials.email }
    });
    if (!promotionManagerIsExist) {
      throw new this.apiError(
        400,
        'Il ne semble pas y avoir de compte sous cet adresse-email ❌'
      );
    } else {
      let checkPassword = await this.bcrypt.compareSync(
        credentials.password,
        promotionManagerIsExist.password
      );
      if (!checkPassword) {
        throw new this.apiError(400, 'Mot de passe incorrect ❌');
      }
      return promotionManagerIsExist;
    }
  }

  async updatePromotionManagerById(id, promotionManager) {
    const promotionManagerIsExist = await this.db.PromotionManager.findOne({
      where: { id: id }
    });
    if (!promotionManagerIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le profil de le/la responsable de formation que vous voulez modifier n'existe pas ❌ "
      );
    }
    return await this.db.PromotionManager.update(
      {
        role: promotionManager.role,
        lastname: promotionManager.lastname,
        firstname: promotionManager.firstname,
        email: promotionManager.email,
        gender: promotionManager.gender
      },
      { where: { id: id } }
    );
  }

  async deletePromotionManagerById(id) {
    const promotionManagerIsExist = await this.db.PromotionManager.findOne({
      where: { id: id }
    });
    if (!promotionManagerIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le profil de le/la responsable de formation que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.PromotionManager.destroy({ where: { id: id } });
  }

  async getPromotionsManagers() {
    const promotionsManagers = await this.db.PromotionManager.findAll({
      attributes: ['id', 'role', 'firstname', 'lastname', 'email', 'gender']
    });
    if (!promotionsManagers.length) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y a aucun/e responsable de formations ❌"
      );
    }
    return promotionsManagers;
  }

  async getPromotionManagerById(id) {
    return await this.db.PromotionManager.findOne({
      attributes: ['id', 'role', 'firstname', 'lastname', 'email', 'gender'],
      where: { id: id }
    });
  }
}
export default PromotionManagerRepository;
