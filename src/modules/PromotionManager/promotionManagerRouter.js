class PromotionManagerRouter {
  constructor({ router, promotionManagerController }) {
    this.router = router;
    this.initializeRoutes({ promotionManagerController });
    return this.router;
  }

  initializeRoutes({ promotionManagerController }) {
    this.router
      .route('/promotionManagers/register')
      .post(promotionManagerController.register);
    this.router
      .route('/promotionManagers/login')
      .post(promotionManagerController.login);
    this.router
      .route('/promotionManagers/:id')
      .patch(promotionManagerController.updatePromotionManagerById);
    this.router
      .route('/promotionManagers/:id')
      .delete(promotionManagerController.deletePromotionManagerById);
    this.router
      .route('/promotionManagers')
      .get(promotionManagerController.getAllPromotionManagers);
    this.router
      .route('/promotionManagers/:id')
      .get(promotionManagerController.getPromotionManagerById);
  }
}
export default PromotionManagerRouter;
