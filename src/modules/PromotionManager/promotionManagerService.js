import PromotionManagerEntity from './promotionManagerEntity';

class PromotionManagerService {
  constructor({ promotionManagerRepository, ApiError }) {
    this.promotionManagerRepository = promotionManagerRepository;
    this.apiError = ApiError;
  }

  async register(promotionManager) {
    const promotionManagerEntity = new PromotionManagerEntity(promotionManager);
    if (!promotionManagerEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!promotionManagerEntity.checkRole()) {
      throw new this.apiError(400, 'le role est incorrect ! ❌');
    }
    if (!promotionManagerEntity.checkEmail()) {
      throw new this.apiError(400, "l'email n'est pas dans le bon format ! ❌");
    }
    if (!promotionManagerEntity.checkPassword()) {
      throw new this.apiError(
        400,
        'le mot de passe doit contenir entre 8 et 15 caractères, une majuscule, un chiffre et un caractère spécial ! ❌'
      );
    }
    if (!promotionManagerEntity.checkGender()) {
      throw new this.apiError(400, 'le genre est incorrect ! ❌');
    }
    if (!promotionManagerEntity.checkDirectorIdExisting()) {
      throw new this.apiError(
        400,
        "Veuillez entrez un ID lié à un directeur pour la création d'un responsable de promotion ! ❌"
      );
    }
    return await this.promotionManagerRepository.createPromotionManager(
      promotionManager
    );
  }

  async login(credentials) {
    return await this.promotionManagerRepository.checkCredentials(credentials);
  }

  async updatePromotionManagerById(id, promotionManager) {
    return await this.promotionManagerRepository.updatePromotionManagerById(
      id,
      promotionManager
    );
  }

  async deletePromotionManagerById(id) {
    return await this.promotionManagerRepository.deletePromotionManagerById(id);
  }

  async getAllPromotionManagers() {
    return await this.promotionManagerRepository.getPromotionsManagers();
  }

  async getPromotionManagerById(id) {
    return await this.promotionManagerRepository.getPromotionManagerById(id);
  }
}

export default PromotionManagerService;
