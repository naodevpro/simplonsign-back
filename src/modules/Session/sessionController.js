class SessionController {
  constructor({ sessionService, jwtService, responseHandler }) {
    this.sessionService = sessionService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  create = async (request, response, next) => {
    try {
      let session = await this.sessionService.create({ ...request.body });
      this.responseHandler(
        response,
        201,
        session,
        session.dataValues.halfDay == 'matin'
          ? `Nouvelle session du ${session.dataValues.halfDay} créée 💥`
          : `Nouvelle session de l'${session.dataValues.halfDay} créée 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  updateSessionById = async (request, response, next) => {
    try {
      let session = await this.sessionService.updateSessionById(
        request.params.id,
        {
          date: request.body.date,
          beginHour: request.body.beginHour,
          endHour: request.body.endHour,
          halfDay: request.body.halfDay,
          teacherId: request.body.teacherId,
          promotionId: request.body.promotionId
        }
      );
      this.responseHandler(
        response,
        200,
        session,
        'La session a bien été mise à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  deleteSessionById = async (request, response, next) => {
    try {
      let session = this.sessionService.deleteSessionById(request.params.id);
      this.responseHandler(
        response,
        200,
        session,
        'La session à bien été supprimée ✅ '
      );
    } catch (err) {
      next(err);
    }
  };

  getAllSessions = async (request, response, next) => {
    try {
      let sessions = await this.sessionService.getAllSessions();
      this.responseHandler(response, 200, sessions, 'Toutes les sessions ✅');
    } catch (err) {
      next(err);
    }
  };

  getSessionById = async (request, response, next) => {
    try {
      let session = await this.sessionService.getSessionById(request.params.id);
      this.responseHandler(response, 200, session);
    } catch (err) {
      next(err);
    }
  };
}

export default SessionController;
