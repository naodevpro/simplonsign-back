'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Session extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Presence, {
        foreignKey: 'sessionId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Code, {
        foreignKey: 'sessionId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.JustifiedAbsence, {
        foreignKey: 'sessionId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Teacher, {
        foreignKey: 'teacherId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Promotion, {
        foreignKey: 'promotionId',
        onDelete: 'CASCADE'
      });
    }
  }
  Session.init(
    {
      date: DataTypes.STRING,
      beginHour: DataTypes.STRING,
      endHour: DataTypes.STRING,
      halfDay: DataTypes.STRING,
      teacherId: DataTypes.UUID,
      promotionId: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'Session'
    }
  );
  return Session;
};
