class SessionEntity {
  constructor({ date, beginHour, endHour, halfDay, promotionId, teacherId }) {
    this.date = date;
    this.beginHour = beginHour;
    this.endHour = endHour;
    this.halfDay = halfDay;
    this.promotionId = promotionId;
    this.teacherId = teacherId;
  }

  checkFieldsValues() {
    if (
      !this.date ||
      !this.beginHour ||
      !this.endHour ||
      !this.halfDay ||
      !this.promotionId ||
      !this.teacherId
    ) {
      return false;
    } else {
      return true;
    }
  }

  checkPromotionIdExisting() {
    if (!this.promotionId) {
      return false;
    } else {
      return true;
    }
  }

  checkTeacherIdExisting() {
    if (!this.teacherId) {
      return false;
    } else {
      return true;
    }
  }
}

export default SessionEntity;
