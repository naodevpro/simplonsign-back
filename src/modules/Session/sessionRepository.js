class SessionRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createSession(session) {
    const sessionIsExist = await this.db.Session.findOne({
      where: { date: session.date, halfDay: session.halfDay }
    });
    if (sessionIsExist) {
      throw new this.apiError(
        400,
        'Une session existe déjà à la même date et dans la même demi-journée ❌'
      );
    } else {
      return await this.db.Session.create(session);
    }
  }

  async updateSessionById(id, session) {
    const sessionIsExist = await this.db.Session.findOne({
      where: { id: id }
    });
    if (!sessionIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la session que vous souhaitez modifier n'existe pas/plus ❌ "
      );
    }
    return await this.db.Session.update(
      {
        date: session.date,
        beginHour: session.beginHour,
        endHour: session.endHour,
        halfDay: session.halfDay,
        teacherId: session.teacherId,
        promotionId: session.promotionId
      },
      { where: { id: id } }
    );
  }

  async deleteSessionById(id) {
    const sessionIsExist = await this.db.Session.findOne({
      where: { id: id }
    });
    if (!sessionIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la session que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Session.destroy({ where: { id: id } });
  }

  async getSessions() {
    const sessions = await this.db.Session.findAll({
      attributes: [
        'id',
        'date',
        'beginHour',
        'endHour',
        'halfDay',
        'teacherId',
        'promotionId'
      ]
    });
    if (!sessions.length) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucunes sessions ❌");
    }
    return sessions;
  }

  async getSessionById(id) {
    return await this.db.Session.findOne({
      attributes: [
        'id',
        'date',
        'beginHour',
        'endHour',
        'halfDay',
        'teacherId',
        'promotionId'
      ],
      where: { id: id }
    });
  }
}
export default SessionRepository;
