class SessionRouter {
  constructor({ router, sessionController }) {
    this.router = router;
    this.initializeRoutes({ sessionController });
    return this.router;
  }

  initializeRoutes({ sessionController }) {
    this.router.route('/sessions/create').post(sessionController.create);
    this.router
      .route('/sessions/:id')
      .patch(sessionController.updateSessionById);
    this.router
      .route('/sessions/:id')
      .delete(sessionController.deleteSessionById);
    this.router.route('/sessions').get(sessionController.getAllSessions);
    this.router.route('/sessions/:id').get(sessionController.getSessionById);
  }
}
export default SessionRouter;
