import SessionEntity from './sessionEntity';

class SessionService {
  constructor({ sessionRepository, ApiError }) {
    this.sessionRepository = sessionRepository;
    this.apiError = ApiError;
  }

  async create(session) {
    const sessionEntity = new SessionEntity(session);
    if (!sessionEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!sessionEntity.checkPromotionIdExisting()) {
      throw new this.apiError(
        400,
        'Veuillez ajouter une promotion liée à la session! ❌'
      );
    }
    if (!sessionEntity.checkTeacherIdExisting()) {
      throw new this.apiError(
        400,
        'Veuillez ajouter un/e formateur.ice lié à la session! ❌'
      );
    }
    return await this.sessionRepository.createSession(session);
  }

  async updateSessionById(id, session) {
    return await this.sessionRepository.updateSessionById(id, session);
  }

  async deleteSessionById(id) {
    return await this.sessionRepository.deleteSessionById(id);
  }

  async getAllSessions() {
    return await this.sessionRepository.getSessions();
  }

  async getSessionById(id) {
    return await this.sessionRepository.getSessionById(id);
  }
}

export default SessionService;
