class StudentController {
  constructor({ studentService, jwtService, responseHandler }) {
    this.studentService = studentService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  register = async (request, response, next) => {
    try {
      let student = await this.studentService.register({ ...request.body });
      this.responseHandler(
        response,
        201,
        student,
        `Bienvenue ${student.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  login = async (request, response, next) => {
    try {
      let student = await this.studentService.login({ ...request.body });
      let token = await this.jwt.generateToken({
        id: student.dataValues.id,
        firstname: student.firstname,
        lastname: student.lastname,
        email: student.dataValues.email,
        role: student.dataValues.role
      });
      response.cookie('auth-cookie', token, {
        secure: true,
        maxAge: 3600000,
        sameSite: 'None'
      });
      this.responseHandler(
        response,
        200,
        {
          id: student.dataValues.id,
          lastname: student.dataValues.lastname,
          firstname: student.dataValues.firstname,
          email: student.dataValues.email,
          role: student.dataValues.role,
          token
        },
        `Bonjour ${student.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  updateStudentById = async (request, response, next) => {
    try {
      let student = await this.studentService.updateStudentById(
        request.params.id,
        {
          role: request.body.role,
          lastname: request.body.lastname,
          firstname: request.body.firstname,
          email: request.body.email,
          gender: request.body.gender
        }
      );
      this.responseHandler(
        response,
        200,
        student,
        'Le profil a bien été mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  deleteStudentById = async (request, response, next) => {
    try {
      let student = this.studentService.deleteStudentById(request.params.id);
      this.responseHandler(
        response,
        200,
        student,
        'Le profil à bien été supprimé ✅ '
      );
    } catch (err) {
      next(err);
    }
  };

  getAllStudents = async (request, response, next) => {
    try {
      let students = await this.studentService.getAllStudents();
      this.responseHandler(response, 200, students, 'Tous les apprenants ✅');
    } catch (err) {
      next(err);
    }
  };

  getStudentById = async (request, response, next) => {
    try {
      let student = await this.studentService.getStudentById(request.params.id);
      this.responseHandler(response, 200, student);
    } catch (err) {
      next(err);
    }
  };
}

export default StudentController;
