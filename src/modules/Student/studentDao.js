'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Promotion, {
        foreignKey: 'promotionId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Presence, {
        foreignKey: 'studentId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.JustifiedAbsence, {
        foreignKey: 'studentId',
        onDelete: 'CASCADE'
      });
    }
  }
  Student.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      role: DataTypes.STRING,
      avatar: DataTypes.STRING,
      firstname: DataTypes.STRING,
      lastname: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      gender: DataTypes.STRING,
      currentCode: DataTypes.STRING,
      promotionId: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'Student'
    }
  );
  return Student;
};
