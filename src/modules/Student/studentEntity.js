class StudentEntity {
  constructor({
    role,
    avatar,
    firstname,
    lastname,
    email,
    password,
    gender,
    currentCode,
    promotionId
  }) {
    this.role = role;
    this.avatar = avatar;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.password = password;
    this.gender = gender;
    this.promotionId = promotionId;
    this.currentCode = currentCode;
  }

  checkFieldsValues() {
    if (
      !this.role ||
      !this.avatar ||
      !this.firstname ||
      !this.lastname ||
      !this.email ||
      !this.password ||
      !this.gender ||
      !this.promotionId ||
      !this.currentCode
    ) {
      return false;
    } else {
      return true;
    }
  }

  checkRole() {
    if (this.role !== 'student') {
      return false;
    } else {
      return true;
    }
  }

  checkEmail() {
    const emailRegexp =
      /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!emailRegexp.test(this.email)) {
      return false;
    } else {
      return true;
    }
  }

  checkPassword() {
    const passwordRegexp =
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (!passwordRegexp.test(this.password)) {
      return false;
    } else {
      return true;
    }
  }

  checkGender() {
    switch (this.gender) {
      case 'masculin':
        return true;
      case 'female':
        return true;
      default:
        return false;
    }
  }

  checkPromotionIdExisting() {
    if (!this.promotionId) {
      return false;
    } else {
      return true;
    }
  }
}

export default StudentEntity;
