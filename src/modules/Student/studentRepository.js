class StudentRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createStudent(student) {
    const salt = this.bcrypt.genSaltSync(10);
    student.password = this.bcrypt.hashSync(student.password, salt);
    const studentIsExist = await this.db.Student.findOne({
      where: { email: student.email }
    });
    if (studentIsExist) {
      throw new this.apiError(
        400,
        'Un formateur existe déjà sous cet adresse e-mail ❌'
      );
    } else {
      return await this.db.Student.create(student);
    }
  }

  async checkCredentials(credentials) {
    const studentIsExist = await this.db.Student.findOne({
      where: { email: credentials.email }
    });
    if (!studentIsExist) {
      throw new this.apiError(
        400,
        'Il ne semble pas y avoir de compte sous cet adresse-email ❌'
      );
    } else {
      let checkPassword = await this.bcrypt.compareSync(
        credentials.password,
        studentIsExist.password
      );
      if (!checkPassword) {
        throw new this.apiError(400, 'Mot de passe incorrect ❌');
      }
      return studentIsExist;
    }
  }

  async updateStudentById(id, student) {
    const studentIsExist = await this.db.Student.findOne({
      where: { id: id }
    });
    const studentEmailIsExist = await this.db.Student.findOne({
      where: { email: student.email }
    });
    if (!studentIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le profil du formateur que vous voulez modifier n'existe pas ❌ "
      );
    } else if (
      studentEmailIsExist &&
      studentEmailIsExist.dataValues.id !== studentIsExist.dataValues.id
    ) {
      throw new this.apiError(
        400,
        "Il semble qu'un email existe déjà pour un autre profil ! ❌ "
      );
    }
    return await this.db.Student.update(
      {
        role: student.role,
        lastname: student.lastname,
        firstname: student.firstname,
        email: student.email,
        gender: student.gender
      },
      { where: { id: id } }
    );
  }

  async deleteStudentById(id) {
    const studentIsExist = await this.db.Student.findOne({
      where: { id: id }
    });
    if (!studentIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le profil du formateur que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Student.destroy({ where: { id: id } });
  }

  async getStudents() {
    const students = await this.db.Student.findAll({
      attributes: [
        'id',
        'role',
        'firstname',
        'lastname',
        'email',
        'gender',
        'currentCode'
      ]
    });
    if (!students.length) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucun formateurs ❌");
    }
    return students;
  }

  async getStudentById(id) {
    return await this.db.Student.findOne({
      attributes: [
        'id',
        'role',
        'firstname',
        'lastname',
        'email',
        'gender',
        'currentCode'
      ],
      where: { id: id }
    });
  }
}
export default StudentRepository;
