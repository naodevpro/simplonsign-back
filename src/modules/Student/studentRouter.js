class StudentRouter {
  constructor({ router, studentController }) {
    this.router = router;
    this.initializeRoutes({ studentController });
    return this.router;
  }

  initializeRoutes({ studentController }) {
    this.router.route('/students/register').post(studentController.register);
    this.router.route('/students/login').post(studentController.login);
    this.router
      .route('/students/:id')
      .patch(studentController.updateStudentById);
    this.router
      .route('/students/:id')
      .delete(studentController.deleteStudentById);
    this.router.route('/students').get(studentController.getAllStudents);
    this.router.route('/students/:id').get(studentController.getStudentById);
  }
}
export default StudentRouter;
