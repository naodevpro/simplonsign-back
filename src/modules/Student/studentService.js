import StudentEntity from './studentEntity';

class StudentService {
  constructor({ studentRepository, ApiError }) {
    this.studentRepository = studentRepository;
    this.apiError = ApiError;
  }

  async register(student) {
    const studentEntity = new StudentEntity(student);
    if (!studentEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!studentEntity.checkRole()) {
      throw new this.apiError(400, 'le role est incorrect ! ❌');
    }
    if (!studentEntity.checkEmail()) {
      throw new this.apiError(400, "l'email n'est pas dans le bon format ! ❌");
    }
    if (!studentEntity.checkPassword()) {
      throw new this.apiError(
        400,
        'le mot de passe doit contenir entre 8 et 15 caractères, une majuscule, un chiffre et un caractère spécial ! ❌'
      );
    }
    if (!studentEntity.checkGender()) {
      throw new this.apiError(400, 'le genre est incorrect ! ❌');
    }
    if (!studentEntity.checkPromotionIdExisting()) {
      throw new this.apiError(
        400,
        "Veuillez synchroniser une promotion à l'apprenant ! ❌"
      );
    }
    return await this.studentRepository.createStudent(student);
  }

  async login(credentials) {
    return await this.studentRepository.checkCredentials(credentials);
  }

  async updateStudentById(id, student) {
    return await this.studentRepository.updateStudentById(id, student);
  }

  async deleteStudentById(id) {
    return await this.studentRepository.deleteStudentById(id);
  }

  async getAllStudents() {
    return await this.studentRepository.getStudents();
  }

  async getStudentById(id) {
    return await this.studentRepository.getStudentById(id);
  }
}

export default StudentService;
