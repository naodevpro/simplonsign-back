class TeacherController {
  constructor({ teacherService, jwtService, responseHandler }) {
    this.teacherService = teacherService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  register = async (request, response, next) => {
    try {
      let teacher = await this.teacherService.register({ ...request.body });
      this.responseHandler(
        response,
        201,
        teacher,
        `Bienvenue ${teacher.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  login = async (request, response, next) => {
    try {
      let teacher = await this.teacherService.login({ ...request.body });
      let token = await this.jwt.generateToken({
        id: teacher.dataValues.id,
        firstname: teacher.firstname,
        lastname: teacher.lastname,
        email: teacher.dataValues.email,
        role: teacher.dataValues.role
      });
      response.cookie('auth-cookie', token, {
        secure: true,
        maxAge: 3600000,
        sameSite: 'None'
      });
      this.responseHandler(
        response,
        200,
        {
          id: teacher.dataValues.id,
          lastname: teacher.dataValues.lastname,
          firstname: teacher.dataValues.firstname,
          email: teacher.dataValues.email,
          role: teacher.dataValues.role,
          token
        },
        `Bonjour ${teacher.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  updateTeacherById = async (request, response, next) => {
    try {
      let teacher = await this.teacherService.updateTeacherById(
        request.params.id,
        {
          role: request.body.role,
          lastname: request.body.lastname,
          firstname: request.body.firstname,
          email: request.body.email,
          gender: request.body.gender
        }
      );
      this.responseHandler(
        response,
        200,
        teacher,
        'Le profil a bien été mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  deleteTeacherById = async (request, response, next) => {
    try {
      let teacher = this.teacherService.deleteTeacherById(request.params.id);
      this.responseHandler(
        response,
        200,
        teacher,
        'Le profil à bien été supprimé ✅ '
      );
    } catch (err) {
      next(err);
    }
  };

  getAllTeachers = async (request, response, next) => {
    try {
      let teachers = await this.teacherService.getAllTeachers();
      this.responseHandler(response, 200, teachers, 'Tous les formateurs ✅');
    } catch (err) {
      next(err);
    }
  };

  getTeacherById = async (request, response, next) => {
    try {
      let teacher = await this.teacherService.getTeacherById(request.params.id);
      this.responseHandler(response, 200, teacher);
    } catch (err) {
      next(err);
    }
  };
}

export default TeacherController;
