'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Teacher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Promotion, {
        foreignKey: 'teacherId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Session, {
        foreignKey: 'teacherId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.PromotionManager, {
        foreignKey: 'promotionManagerId',
        onDelete: 'CASCADE'
      });
    }
  }
  Teacher.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      role: DataTypes.STRING,
      avatar: DataTypes.STRING,
      firstname: DataTypes.STRING,
      lastname: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      gender: DataTypes.STRING,
      promotionManagerId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Teacher'
    }
  );
  return Teacher;
};
