class TeacherRepository {
  constructor({ db, bcrypt, ApiError }) {
    this.db = db;
    this.bcrypt = bcrypt;
    this.apiError = ApiError;
  }

  async createTeacher(teacher) {
    const salt = this.bcrypt.genSaltSync(10);
    teacher.password = this.bcrypt.hashSync(teacher.password, salt);
    const teacherIsExist = await this.db.Teacher.findOne({
      where: { email: teacher.email }
    });
    if (teacherIsExist) {
      throw new this.apiError(
        400,
        'Un formateur existe déjà sous cet adresse e-mail ❌'
      );
    } else {
      return await this.db.Teacher.create(teacher);
    }
  }

  async checkCredentials(credentials) {
    const teacherIsExist = await this.db.Teacher.findOne({
      where: { email: credentials.email }
    });
    if (!teacherIsExist) {
      throw new this.apiError(
        400,
        'Il ne semble pas y avoir de compte sous cet adresse-email ❌'
      );
    } else {
      let checkPassword = await this.bcrypt.compareSync(
        credentials.password,
        teacherIsExist.password
      );
      if (!checkPassword) {
        throw new this.apiError(400, 'Mot de passe incorrect ❌');
      }
      return teacherIsExist;
    }
  }

  async updateTeacherById(id, teacher) {
    const teacherIsExist = await this.db.Teacher.findOne({
      where: { id: id }
    });
    const teacherEmailIsExist = await this.db.Teacher.findOne({
      where: { email: teacher.email }
    });
    if (!teacherIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le profil du formateur que vous voulez modifier n'existe pas ❌ "
      );
    } else if (
      teacherEmailIsExist &&
      teacherEmailIsExist.dataValues.id !== teacherIsExist.dataValues.id
    ) {
      throw new this.apiError(
        400,
        "Il semble qu'un email existe déjà pour un autre profil ! ❌ "
      );
    }
    return await this.db.Teacher.update(
      {
        role: teacher.role,
        lastname: teacher.lastname,
        firstname: teacher.firstname,
        email: teacher.email,
        gender: teacher.gender
      },
      { where: { id: id } }
    );
  }

  async deleteTeacherById(id) {
    const teacherIsExist = await this.db.Teacher.findOne({
      where: { id: id }
    });
    if (!teacherIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le profil du formateur que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Teacher.destroy({ where: { id: id } });
  }

  async getTeachers() {
    const teachers = await this.db.Teacher.findAll({
      attributes: ['id', 'role', 'firstname', 'lastname', 'email', 'gender']
    });
    if (!teachers.length) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucun formateurs ❌");
    }
    return teachers;
  }

  async getTeacherById(id) {
    return await this.db.Teacher.findOne({
      attributes: ['id', 'role', 'firstname', 'lastname', 'email', 'gender'],
      where: { id: id }
    });
  }
}
export default TeacherRepository;
