class TeacherRouter {
  constructor({ router, teacherController }) {
    this.router = router;
    this.initializeRoutes({ teacherController });
    return this.router;
  }

  initializeRoutes({ teacherController }) {
    this.router.route('/teachers/register').post(teacherController.register);
    this.router.route('/teachers/login').post(teacherController.login);
    this.router
      .route('/teachers/:id')
      .patch(teacherController.updateTeacherById);
    this.router
      .route('/teachers/:id')
      .delete(teacherController.deleteTeacherById);
    this.router.route('/teachers').get(teacherController.getAllTeachers);
    this.router.route('/teachers/:id').get(teacherController.getTeacherById);
  }
}
export default TeacherRouter;
