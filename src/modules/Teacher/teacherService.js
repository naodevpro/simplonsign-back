import TeacherEntity from './teacherEntity';

class TeacherService {
  constructor({ teacherRepository, ApiError }) {
    this.teacherRepository = teacherRepository;
    this.apiError = ApiError;
  }

  async register(teacher) {
    const teacherEntity = new TeacherEntity(teacher);
    if (!teacherEntity.checkFieldsValues()) {
      throw new this.apiError(400, 'Il y a des champs manquant ! ❌');
    }
    if (!teacherEntity.checkRole()) {
      throw new this.apiError(400, 'le role est incorrect ! ❌');
    }
    if (!teacherEntity.checkEmail()) {
      throw new this.apiError(400, "l'email n'est pas dans le bon format ! ❌");
    }
    if (!teacherEntity.checkPassword()) {
      throw new this.apiError(
        400,
        'le mot de passe doit contenir entre 8 et 15 caractères, une majuscule, un chiffre et un caractère spécial ! ❌'
      );
    }
    if (!teacherEntity.checkGender()) {
      throw new this.apiError(400, 'le genre est incorrect ! ❌');
    }
    if (!teacherEntity.checkPromotionManagerIdExisting()) {
      throw new this.apiError(
        400,
        'Veuillez renseigner un/e responsable de promotion à lié au formateur ! ❌'
      );
    }
    return await this.teacherRepository.createTeacher(teacher);
  }

  async login(credentials) {
    return await this.teacherRepository.checkCredentials(credentials);
  }

  async updateTeacherById(id, teacher) {
    return await this.teacherRepository.updateTeacherById(id, teacher);
  }

  async deleteTeacherById(id) {
    return await this.teacherRepository.deleteTeacherById(id);
  }

  async getAllTeachers() {
    return await this.teacherRepository.getTeachers();
  }

  async getTeacherById(id) {
    return await this.teacherRepository.getTeacherById(id);
  }
}

export default TeacherService;
