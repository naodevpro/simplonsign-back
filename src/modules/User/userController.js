class UserController {
  constructor({ userService, jwtService, responseHandler }) {
    this.userService = userService;
    this.jwt = jwtService;
    this.responseHandler = responseHandler;
  }

  me = async (request, response, next) => {
    try {
      const token = await this.jwt.decodeToken(request.cookies['auth-cookie']);
      const user = await this.userService.me(token.id, token.role);
      this.responseHandler(
        response,
        200,
        user,
        "L'utilisateur est bien connecté ✅"
      );
    } catch (err) {
      next(err);
    }
  };

  logout = async (request, response, next) => {
    try {
      response.clearCookie('auth-cookie', {
        sameSite: 'none',
        httpOnly: false,
        secure: true
      });
      this.responseHandler(response, 200, {}, 'Utilisateur déconnecté 🔐');
    } catch (err) {
      next(err);
    }
  };
}

export default UserController;
