class UserRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getUserById(id, role) {
    console.log(id, role);
    switch (role) {
      case 'director':
        return await this.db.Director.findOne({
          attributes: [
            'id',
            'role',
            'firstname',
            'lastname',
            'email',
            'gender'
          ],
          where: { id: id }
        });
      case 'promotionManager':
        return await this.db.PromotionManager.findOne({
          attributes: [
            'id',
            'role',
            'firstname',
            'lastname',
            'email',
            'gender'
          ],
          where: { id: id }
        });
      case 'teacher':
        return await this.db.Teacher.findOne({
          attributes: [
            'id',
            'role',
            'firstname',
            'lastname',
            'email',
            'gender'
          ],
          where: { id: id }
        });
      case 'student':
        return await this.db.Student.findOne({
          attributes: [
            'id',
            'role',
            'firstname',
            'lastname',
            'email',
            'gender'
          ],
          where: { id: id }
        });
      default:
        break;
    }
  }
}
export default UserRepository;
