class UserRouter {
  constructor({ router, userController }) {
    this.router = router;
    this.initializeRoutes({ userController });
    return this.router;
  }

  initializeRoutes({ userController }) {
    this.router.route('/me').get(userController.me);
    this.router.route('/logout').post(userController.logout);
  }
}
export default UserRouter;
