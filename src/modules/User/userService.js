class UserService {
  constructor({ userRepository, ApiError }) {
    this.userRepository = userRepository;
    this.apiError = ApiError;
  }

  async me(id, role) {
    const user = await this.userRepository.getUserById(id, role);
    if (!user)
      throw new this.apiError(
        400,
        "l'utilisateur demandé n'existe pas sous cet ID ❌"
      );
    return user;
  }
}

export default UserService;
